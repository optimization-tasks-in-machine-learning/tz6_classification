# ТЗ6 Классификация

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz6_classification.pdf](illustrations/tz6_classification.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1XpL-ujdqB1UTP-CWAOZ2I8aSbtKnbQ5j?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz6_classification.ipynb](tz6_classification.ipynb)

## Иллюстрация работы

* Решение и вывод <br/>
![Output](illustrations/output.png) <br/>
* Продвинутая визуализация <br/>
![Visualization](illustrations/visualization.gif) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz6_classification.git
# Импорт всех функций из модуля
from tz6_classification.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module module tz6_classification.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz6_classification.utils:

visualize_plotly(X: Union[list, numpy.ndarray], y: Union[list, numpy.ndarray], ground_truth: Union[list, numpy.ndarray], predictions: Union[list, numpy.ndarray], input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
    Визуализация полученной функции с предикторами. 
    
    Parameters
    ===========
    
    X: list, numpy.ndarray
        Массив кординат по x
    y: list, numpy.ndarray
        Массив кординат по y
    ground_truth: list, numpy.ndarray
        Массив истинных предсказаний
    predictions: list, numpy.ndarray
        Массив предсказаний модели
    input_investigated_function: str
        Исследуемая функция
    functions_symbols: tuple, optional
        Переменные функции
        Массив предсказываемой переменной
    is_display_input: bool
        Отображать входную функцию или нет
    start: int, float
        Начало графика
    stop: int, float
        Конец графика
    detail: int, float
        Детализация графика
```

`help(classification)`

```
Help on function classification in module tz6_classification.utils:

classification(X_train: Union[list, numpy.ndarray], y_train: Union[list, numpy.ndarray], X_test: Union[list, numpy.ndarray], y_test: Union[list, numpy.ndarray], classification_method: Union[int, str], regularization_method: Union[int, str, NoneType] = None, accuracy: float = 0.0001, max_iterations: int = 1000, is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Классификация с заданными параметрами. 
    
    Parameters
    ===========
    
    X_train: list, numpy.ndarray
        Массив обучающей выборки
    y_train: list, numpy.ndarray
        Массив предсказываемой переменной
    X_test: list, numpy.ndarray
        Массив тестовой выборки
    y_test: list, numpy.ndarray
        Массив предсказываемой переменной
    classification_method: int, str
        Методы классификации
        0, 'log', 'logistic': на основе логистической регрессии
        1, 'svm', 'support_vector': на основе метода опорных векторов
    regularization_method: int, str, optional
        Методы регуляризации
        0, 'l1': L1 регуляризация
        1, 'l2': L2 регуляризация
        2, 'elasticnet': L1 + L2 регуляризация, 
            только в логистической регрессии
    accuracy: float
        Точность
    max_iterations: int
        Максимум итераций
    is_display_input: bool = False
        Отображать входные данные или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет
    is_try_visualize: bool = False
        Визуализация процесса
    
    Returns
    ===========
    
    predictions: list
        Получившееся функция
    coefficients: list
        Массив коэффициентов
    bias: float
        Смещение
    accuracy: float
        Точность
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)

